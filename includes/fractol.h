/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fractol.h                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/24 17:54:02 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/07 18:09:20 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FRACTOL_H
# define FRACTOL_H

# include <stdlib.h>
# include <math.h>
# include <unistd.h>
# include "mlx.h"
# include "pthread.h"
# include "libft.h"

# define THREADS 16
# define SIZE 1024
# define COLOR_1 0x481245
# define COLOR_2 0x0F5504
# define COLOR_3 0x2199F0
# define COLOR_4 0x0B68EE

typedef struct	s_cnum
{
	double		rl;
	double		im;
}				t_cnum;

typedef struct	s_mlx
{
	int			fractal;
	void		*mlx_ptr;
	void		*win_ptr;
	void		*img_ptr;
	int			*image;
	int			bits;
	int			colms;
	int			endian;
	int			iterations;
	int			movement;
	int			x_prev;
	int			y_prev;
	int			y_first;
	int			color;
	int			julia_m;
	double		zoom;
	t_cnum		julia;
	t_cnum		center;
}				t_mlx;

void			draw_julia(t_mlx *mlx);
void			draw_mandelbrot(t_mlx *mlx);
void			draw_spider(t_mlx *mlx);
void			draw_burning_ship(t_mlx *mlx);

void			init_draw(t_mlx *mlx);
void			redraw(t_mlx *mlx);

int				close_window(void *param);
int				key_press(int keycode, t_mlx *mlx);
int				mouse_press(int button, int x, int y, t_mlx *mlx);
int				mouse_release(int keycode, int x, int y, t_mlx *mlx);
int				mouse_move(int x, int y, t_mlx *mlx);

#endif
