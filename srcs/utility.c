/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   utility.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/27 16:16:48 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/07 18:55:16 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void	check_hooks(t_mlx *mlx)
{
	mlx_hook(mlx->win_ptr, 2, 0, key_press, mlx);
	mlx_hook(mlx->win_ptr, 4, 0, mouse_press, mlx);
	mlx_hook(mlx->win_ptr, 5, 0, mouse_release, mlx);
	mlx_hook(mlx->win_ptr, 6, 0, mouse_move, mlx);
	mlx_hook(mlx->win_ptr, 17, 0, close_window, mlx);
}

static void	*choose_fractal(t_mlx *mlx)
{
	if (mlx->fractal == 1)
		return (draw_mandelbrot);
	if (mlx->fractal == 2)
		return (draw_julia);
	if (mlx->fractal == 3)
		return (draw_spider);
	if (mlx->fractal == 4)
		return (draw_burning_ship);
	return (NULL);
}

static void	parallelize(t_mlx *mlx)
{
	int			i;
	t_mlx		mlx_array[THREADS];
	void		*func_ptr;
	pthread_t	threads[THREADS];

	func_ptr = choose_fractal(mlx);
	i = 0;
	while (i < THREADS)
	{
		ft_memcpy(&(mlx_array[i]), mlx, sizeof(t_mlx));
		mlx_array[i].y_first = SIZE / THREADS * i;
		pthread_create(&threads[i], NULL, func_ptr, &(mlx_array[i]));
		i++;
	}
	i = 0;
	while (i < THREADS)
		pthread_join(threads[i++], NULL);
}

void		redraw(t_mlx *mlx)
{
	mlx_destroy_image(mlx->mlx_ptr, mlx->img_ptr);
	mlx->img_ptr = mlx_new_image(mlx->mlx_ptr, SIZE, SIZE);
	parallelize(mlx);
	mlx_clear_window(mlx->mlx_ptr, mlx->win_ptr);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win_ptr, mlx->img_ptr, 0, 0);
}

void		init_draw(t_mlx *mlx)
{
	mlx->mlx_ptr = mlx_init();
	mlx->win_ptr = mlx_new_window(mlx->mlx_ptr, SIZE, SIZE, "fractol");
	mlx->img_ptr = mlx_new_image(mlx->mlx_ptr, SIZE, SIZE);
	mlx->image = (int *)(mlx_get_data_addr(mlx->img_ptr, &(mlx->bits),
					&(mlx->colms), &(mlx->endian)));
	parallelize(mlx);
	mlx_put_image_to_window(mlx->mlx_ptr, mlx->win_ptr, mlx->img_ptr, 0, 0);
	check_hooks(mlx);
	mlx_loop(mlx->mlx_ptr);
}
