/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mouse.c                                            :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 16:42:39 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/07 18:50:36 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

int			mouse_move(int x, int y, t_mlx *mlx)
{
	if (mlx->movement == 0 && (x >= 0 && x < SIZE) && (y >= 0 && y < SIZE) &&
		mlx->julia_m == 1 && mlx->fractal == 2)
	{
		mlx->julia.rl = -2 + x * 4.0 / SIZE;
		mlx->julia.im = 2 - y * 4.0 / SIZE;
		redraw(mlx);
	}
	if (mlx->movement == 1)
	{
		mlx->center.rl -= (x - mlx->x_prev) / (mlx->x_prev * mlx->zoom);
		mlx->center.im -= (y - mlx->y_prev) / (mlx->y_prev * mlx->zoom);
		mlx->x_prev = x;
		mlx->y_prev = y;
		redraw(mlx);
	}
	return (0);
}

static int	mouse_zoom(int button, int x, int y, t_mlx *mlx)
{
	if (button == 4)
	{
	    mlx->zoom *= 1.1;
		mlx->center.rl -= ((SIZE / 2 - x) / (SIZE / 2 * mlx->zoom)) * 0.1;
		mlx->center.im -= ((SIZE / 2 - y) / (SIZE / 2 * mlx->zoom)) * 0.1;
	}
	if (button == 5)
	{
		mlx->center.rl += ((SIZE / 2 - x) / (SIZE / 2 * mlx->zoom)) * 0.1;
		mlx->center.im += ((SIZE / 2 - y) / (SIZE / 2 * mlx->zoom)) * 0.1;
		mlx->zoom *= 1 / 1.1;
	}
	return (0);
}

int			mouse_release(int keycode, int x, int y, t_mlx *mlx)
{
	if (keycode == 1)
		mlx->movement = 0;
	return (x + y);
}

int			mouse_press(int button, int x, int y, t_mlx *mlx)
{
	if (button == 1)
	{
		mlx->movement = 1;
		mlx->x_prev = x;
		mlx->y_prev = y;
	}
	mouse_zoom(button, x, y, mlx);
	redraw(mlx);
	return (0);
}
