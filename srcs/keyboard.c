/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   keyboard.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2019/01/25 16:58:48 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/07 18:20:16 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		color_select(int keycode, t_mlx *mlx)
{
	if (keycode == 18)
		mlx->color = 1;
	if (keycode == 19)
		mlx->color = 2;
	if (keycode == 20)
		mlx->color = 3;
	if (keycode == 21)
		mlx->color = 4;
}

static void		arrow_movement(int keycode, t_mlx *mlx)
{
	if (keycode == 123)
		mlx->center.rl -= 0.04 / mlx->zoom;
	if (keycode == 124)
		mlx->center.rl += 0.04 / mlx->zoom;
	if (keycode == 125)
		mlx->center.im += 0.04 / mlx->zoom;
	if (keycode == 126)
		mlx->center.im -= 0.04 / mlx->zoom;
}

static void		select_fractal(int keycode, t_mlx *mlx)
{
	int flag;

	flag = 0;
	if (keycode == 83)
	{
		mlx->fractal = 1;
		flag = 1;
	}
	else if (keycode == 84)
	{
		mlx->fractal = 2;
		flag = 1;
	}
	else if (keycode == 85)
	{
		mlx->fractal = 3;
		flag = 1;
	}
	else if (keycode == 86)
	{
		mlx->fractal = 4;
		flag = 1;
	}
	if (flag == 1)
		redraw(mlx);
}

int				close_window(void *param)
{
	(void)param;
	exit(0);
	return (0);
}

int				key_press(int keycode, t_mlx *mlx)
{
	if (keycode == 49)
	{
		mlx->center.im = 0.0;
		mlx->center.rl = 0.0;
		mlx->zoom = 0.5;
	}
	if (keycode == 36)
	{
		if (mlx->julia_m == 1)
			mlx->julia_m = 0;
		else
			mlx->julia_m = 1;
	}
	if (keycode == 78)
		mlx->iterations -= 5;
	if (keycode == 69)
		mlx->iterations += 5;
	if (keycode == 53)
		exit(0);
	select_fractal(keycode, mlx);
	arrow_movement(keycode, mlx);
	color_select(keycode, mlx);
	redraw(mlx);
	return (0);
}
