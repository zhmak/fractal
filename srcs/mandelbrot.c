/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   mandelbrot.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/27 16:44:46 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/07 17:38:09 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static int	check_converge(t_cnum c, int *iter, t_mlx *mlx)
{
	int		i;
	t_cnum	z;
	double	buff;

	i = 1;
	z.rl = 0;
	z.im = 0;
	while (i <= (mlx->iterations))
	{
		buff = z.rl;
		z.rl = z.rl * z.rl - z.im * z.im;
		z.im = buff * z.im + z.im * buff;
		z.rl = z.rl + c.rl;
		z.im = z.im + c.im;
		if (z.rl * z.rl + z.im * z.im > 4.0)
		{
			*iter = i;
			return (0);
		}
		i++;
	}
	return (1);
}

static void	pixel_to_image(t_mlx *mlx, int x, int y, t_cnum num)
{
	int		iter;
	int		color;

	iter = 0;
	if (!(check_converge(num, &iter, mlx)))
	{
		color = 0;
		if (mlx->color == 1)
			color = COLOR_1 * iter;
		else if (mlx->color == 2)
			color = COLOR_2 * iter;
		else if (mlx->color == 3)
			color = COLOR_3 * iter;
		else if (mlx->color == 4)
			color = COLOR_4 * iter;
		mlx->image[x + y * SIZE] = color;
	}
}

void		draw_mandelbrot(t_mlx *mlx)
{
	t_cnum	num;
	int		y_last;
	int		x;

	y_last = mlx->y_first + SIZE / THREADS;
	while (mlx->y_first < y_last)
	{
		num.im = (mlx->y_first - SIZE / 2) / (SIZE / 2 * mlx->zoom)
				+ mlx->center.im;
		x = 0;
		while (x < SIZE)
		{
			num.rl = (x - SIZE / 2) / (SIZE / 2 * mlx->zoom) + mlx->center.rl;
			pixel_to_image(mlx, x, mlx->y_first, num);
			x++;
		}
		mlx->y_first++;
	}
}
