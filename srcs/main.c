/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: eloren-l <eloren-l@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/12/24 18:14:17 by eloren-l          #+#    #+#             */
/*   Updated: 2019/02/07 18:16:43 by eloren-l         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fractol.h"

static void		init_values(t_mlx *mlx)
{
	mlx->center.im = 0.0;
	mlx->center.rl = 0.0;
	mlx->zoom = 0.5;
	mlx->iterations = 50;
	mlx->julia.im = 0.0;
	mlx->julia.rl = 0.0;
	mlx->movement = 0;
	mlx->color = 1;
	mlx->julia_m = 1;
}

static void		error_check(t_mlx *mlx, char *argv)
{
	if (ft_strcmp(argv, "mandelbrot") == 0)
	{
		mlx->fractal = 1;
		return ;
	}
	if (ft_strcmp(argv, "julia") == 0)
	{
		mlx->fractal = 2;
		return ;
	}
	if (ft_strcmp(argv, "spider") == 0)
	{
		mlx->fractal = 3;
		return ;
	}
	if (ft_strcmp(argv, "burning_ship") == 0)
	{
		mlx->fractal = 4;
		return ;
	}
	ft_putstr("usage: ./fractol mandelbrot | julia | spider | burning_ship\n");
	exit(0);
}

int				main(int argc, char **argv)
{
	t_mlx	mlx;

	if (argc != 2)
	{
		ft_putstr("usage: ./fractol mandelbrot | julia | spider | \
burning_ship\n");
		exit(0);
	}
	error_check(&mlx, argv[1]);
	init_values(&mlx);
	init_draw(&mlx);
	return (0);
}
